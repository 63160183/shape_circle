/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kasama.shapeproject;

/**
 *
 * @author User
 */
public class Square {
    private double side;
    public Square(double side){
        this.side = side;
    }
    public double calSquare(){
        return side*side;
    }
    public double getSide(){
        return side;
    }
    public void setS(double side){
        if(side <= 0){
            System.out.println("Error: AreaSquare must more than zero!!");
            return;
        }
        this.side = side;
    }
    @Override
    public String toString(){
        return "Area of square1(side = " + this.getSide() + ") is " + this.calSquare();
    }

}
