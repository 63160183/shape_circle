/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kasama.shapeproject;

/**
 *
 * @author User
 */
public class TestTriangle {
    public static void main(String[] args) {
        Triangle triangle1 = new Triangle(3,7);
        System.out.println("Area of triangle1(Base = " + triangle1.getBase() + "), (High = " + triangle1.getHigh() + ") is "+ triangle1.calTri());
        triangle1.setBase(4);
        System.out.println("Area of triangle1(Base = " + triangle1.getBase() + "), (High = " + triangle1.getHigh() + ") is "+ triangle1.calTri());
        triangle1.setBase(0);
        System.out.println("Area of triangle1(Base = " + triangle1.getBase() + "), (High = " + triangle1.getHigh() + ") is "+ triangle1.calTri());
        triangle1.setHigh(12);
        System.out.println("Area of triangle1(Base = " + triangle1.getBase() + "), (High = " + triangle1.getHigh() + ") is "+ triangle1.calTri());
    }
}
