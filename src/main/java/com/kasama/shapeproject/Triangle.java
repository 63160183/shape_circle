/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kasama.shapeproject;

/**
 *
 * @author User
 */
public class Triangle {
    private double Base;
    private double High;
    public static final double a = 0.5;
    public Triangle(double Base, double High){
        this.Base = Base;
        this.High = High;
    }
    public double calTri(){
        return a*Base*High;
    }
    public double getBase(){
        return Base;
    }
    public double getHigh(){
        return High;
    }
    public void setBase(double Base){
        if(Base <= 0){
            System.out.println("Error: AreaTriangle must more than zero!!");
            return;
        }
        this.Base = Base;
    }
    public void setHigh(double High){
        if(High <= 0){
            System.out.println("Error: AreaTriangle must more than zero!!");
            return;
        }
        this.High = High;
    }
}
